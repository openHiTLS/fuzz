### openHiTLS-FUZZ

This fuzz project is white-box fuzz verification based on the openHiTLS open source interface

##### Development

##### Environment dependencies

Open source images: m.daocloud.io/gcr.io/oss-fuzz-base/base-builder

###### Dependency preparation

1. Pull an open-source image

```shell
docker pull m.daocloud.io/gcr.io/oss-fuzz-base/base-builder
```

2. Download the fuzz project

```shell
git clone https://gitcode.com/openHiTLS/fuzz.git
git clone https://gitcode.com/openhitls/openhitls.git
git clone https://gitee.com/openeuler/libboundscheck openhitls/platform/Secure_C
```

3. Use case building

```shell
By default, no parameter passing mode: bash build.sh
For more information, see bash build.sh -help
```

4. How to add use cases

```c++
/* 
 * The src directory contains fuzz use cases for each feature
 * If there are no features to be measured, you can create a new feature directory under src and output a .cc test file
 * Add new features to the CMakeLists.txt file
 * It should be noted that the test files are all files that start with fuzz
 * After the test file is exported, you need to add an initial seed file in the corpus directory for the later data variation source
 * Next, please refer to: ./src/asn1/fuzz_asn1_decodeitem.cc
 */
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *data1 = data;
    BSL_ASN1_Buffer asnItem = {0}; 
    BSL_ASN1_DecodeItem(&data, &dataLen, &asnItem);
    BSL_SAL_FREE(data1);
    BSL_GLOBAL_DeInit();
    return 0;
}
```

