/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include "bsl_sal.h"
#include "crypt_errno.h"
#include "crypt_eal_rand.h"

int IsValidHexChar(char c)
{
    if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) {
        return 0;
    }
    return 1;
}

int ConvertHex(const uint8_t *buf, uint32_t bufLen, uint8_t **output, uint32_t *outputLen)
{
    if (bufLen < 2) {
        *outputLen = 0;
        return 1;
    }
    // Length of the hex string/2 = Length of the byte stream
    uint32_t len = bufLen / 2;
    uint8_t *tmp = (uint8_t *)BSL_SAL_Malloc(len * sizeof(uint8_t));
    if (tmp == nullptr) {
        return 1;
    }
    // Every 2 bytes in a group
    for (uint32_t i = 0; i < 2 * len; i += 2) {
        if ((IsValidHexChar(buf[i]) == 1) || (IsValidHexChar(buf[i + 1]) == 1)) {
            goto hex_error;
        }
        // hex to int formulas: (Hex % 32 + 9) % 25 = int, hex
        tmp[i / 2] = (buf[i] % 32 + 9) % 25 * 16 + (buf[i + 1] % 32 + 9) % 25;
    }
    *output = tmp;
    *outputLen = len;
    return 0;

hex_error:
    BSL_SAL_FREE(tmp);
    return 1;
}

extern "C" int LLVMFuzzerInitialize(int argc, char ***argv)
{
    int ret = CRYPT_EAL_RandInit(CRYPT_RAND_SHA256, nullptr, nullptr, nullptr, 0);
    if (ret != CRYPT_SUCCESS) {
        return 0;
    } 
    return 1;
}