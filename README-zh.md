### openHiTLS-FUZZ

该fuzz工程基于openHiTLS开源接口做的白盒fuzz验证

##### 开发

##### 环境依赖

开源镜像：m.daocloud.io/gcr.io/oss-fuzz-base/base-builder

###### 依赖准备

1、拉取开源镜像

```shell
docker pull m.daocloud.io/gcr.io/oss-fuzz-base/base-builder
```

2、下载fuzz工程

```shell
git clone https://gitcode.com/openHiTLS/fuzz.git
git clone https://gitcode.com/openhitls/openhitls.git
git clone https://gitee.com/openeuler/libboundscheck openhitls/platform/Secure_C
```

3、用例构建

```shell
默认不传参方式：bash build.sh
传参方式，详见：bash build.sh -help
```

4、如何添加用例

```c++
/* 
 * src目录存放的各特性的fuzz用例
 * 如果没有要测的特性，可以在src下新建特性目录，并输出.cc测试文件
 * 将新增特性添加到CMakeLists.txt文件中
 * 需要注意的是测试文件都是以fuzz开头的文件
 * 输出测试文件后需要在corpus目录下新增初始种子文件，用于后期的数据变异来源
 * 接下来可以参考：./src/asn1/fuzz_asn1_decodeitem.cc
 */
extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *data1 = data;
    BSL_ASN1_Buffer asnItem = {0}; 
    BSL_ASN1_DecodeItem(&data, &dataLen, &asnItem);
    BSL_SAL_FREE(data1);
    BSL_GLOBAL_DeInit();
    return 0;
}
```

