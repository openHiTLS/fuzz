/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();
    const char *pwd = "123456";
    uint32_t pwdlen = strlen(pwd);
    CRYPT_Pbkdf2Param param = {0};
    param.pbesId = BSL_CID_PBES2;
    param.pbkdfId = BSL_CID_PBKDF2;
    param.hmacId = CRYPT_MAC_HMAC_SHA256;
    param.symId = CRYPT_CIPHER_AES256_CBC;
    param.pwd = (uint8_t *)pwd;
    param.saltLen = 16;
    param.pwdLen = pwdlen;
    param.itCnt = 1000;
    CRYPT_EncodeParam paramEx = {CRYPT_DERIVE_PBKDF2, &param};
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_Buffer input = {data, dataLen};
    BSL_Buffer encode = {0};
    HITLS_PKCS12_EncodeContentInfo(&input, BSL_CID_ENCRYPTEDDATA, &paramEx, &encode);
    BSL_SAL_Free(encode.data);
    BSL_SAL_Free(data);

    uint8_t *data1 = nullptr;
    uint32_t dataLen1 = 0;
    if (ConvertHex(buf, len, &data1, &dataLen1) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_Buffer input1 = {data1, dataLen1};
    BSL_Buffer encode1 = {0};
    HITLS_PKCS12_EncodeContentInfo(&input1, BSL_CID_DATA, &paramEx, &encode1);
    BSL_SAL_Free(encode1.data);
    BSL_SAL_Free(data1);
    BSL_GLOBAL_DeInit();
    return 0;
}