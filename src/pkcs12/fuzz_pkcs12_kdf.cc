/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


static uint8_t g_salt[] = {0xed, 0x47, 0xd6, 0xa6, 0x7b, 0x24, 0x59, 0x84};

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    BSL_GLOBAL_Init();
    uint8_t *pwd = nullptr;
    uint32_t pwdLen = 0;
    if (ConvertHex(buf, len, &pwd, &pwdLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *salt = (uint8_t *)BSL_SAL_Calloc(1, sizeof(g_salt));
    if (salt == nullptr) {
        BSL_SAL_Free(pwd);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    memcpy_s(salt, sizeof(g_salt), g_salt, sizeof(g_salt));
    HITLS_PKCS12_MacData *macData = HITLS_PKCS12_MacDataNew();
    if (macData == nullptr) {
        BSL_SAL_Free(pwd);
        BSL_SAL_Free(salt);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    macData->macSalt->data = salt;
    macData->macSalt->dataLen = sizeof(g_salt);
    macData->iteration = 6;
    uint8_t outData[256] = {0};
    BSL_Buffer output = {outData, 256};
    BslCid alg[4] = {BSL_CID_SHA224, BSL_CID_SHA256, BSL_CID_SHA384, BSL_CID_SHA512};
    for (size_t i = 0; i < sizeof(alg) / sizeof(alg[0]); i++) {
        macData->alg = alg[i];
        HITLS_PKCS12_KDF(&output, pwd, pwdLen, HITLS_PKCS12_KDF_MACKEY_ID, macData);
    }
    HITLS_PKCS12_MacDataFree(macData);
    BSL_SAL_Free(pwd);
    BSL_GLOBAL_DeInit();
    return 0;
}