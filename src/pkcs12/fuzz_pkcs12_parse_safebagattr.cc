/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


static void LLVMFuzzerAttributesFree(void *attribute)
{
    HITLS_PKCS12_SafeBagAttr *input = (HITLS_PKCS12_SafeBagAttr *)attribute;
    BSL_SAL_FREE(input->attrValue->data);
    BSL_SAL_FREE(input->attrValue);
    BSL_SAL_FREE(input);
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    BSL_GLOBAL_Init();
    BSL_ASN1_List *attrbutes = BSL_LIST_New(sizeof(HITLS_PKCS12_SafeBagAttr));
    if (attrbutes == nullptr) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_SAL_FREE(attrbutes);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_ASN1_Buffer asn = {
        BSL_ASN1_TAG_CONSTRUCTED | BSL_ASN1_TAG_SET,
        dataLen,
        data,
    };
    HITLS_PKCS12_ParseSafeBagAttr(&asn, attrbutes);
    BSL_LIST_DeleteAll(attrbutes, LLVMFuzzerAttributesFree);
    BSL_SAL_FREE(attrbutes);
    BSL_SAL_FREE(data);
    BSL_GLOBAL_DeInit();
    return 0;
}