/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    HITLS_PKCS12_MacData *macData = HITLS_PKCS12_MacDataNew();
    if (macData == nullptr) {
        BSL_SAL_FREE(data);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_Buffer buff = {data, dataLen};
    HITLS_PKCS12_ParseMacData(&buff, macData);
    BSL_SAL_FREE(data);
    HITLS_PKCS12_MacDataFree(macData);
    BSL_GLOBAL_DeInit();
    return 0;
}