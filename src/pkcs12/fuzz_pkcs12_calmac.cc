/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


static const uint8_t g_salt[] = {0xed, 0x47, 0xd6, 0xa6, 0x7b, 0x24, 0x59, 0x84};
static const char *g_initData = "308203EB3082029A06092A864886F70D010706A082028B308202870201003082028006092A864886F7"
                                "0D010701305F06092A864886F70D01050D3052303106092A864886F70D01050C302404107ADE28DE60"
                                "883CF3A1B12661D26ACD6602020800300C06082A864886F70D02090500301D06096086480165030401"
                                "2A041041980DBF42C62C55FE2275EE8B98666C808202104502DC74FAD09DC7C03A3EDDD5A9F257CB34"
                                "96E05DAD27E1BE1148F2B6C94C300DDED464D02BD404FC4CC205705CF8015B5FD5633E868C505B8926"
                                "BD05A06AE346E57D9BC8578EF39A5272ECB8C51D9ECEC895CAFE7BE90BCF9FBEB3B74F0044C3FED90B"
                                "9DE1ACE42623A7D2598E6D4E96266516F50237B80164662E8F231F41E30B117D6C237672DB80486422"
                                "F95B0FC0EB4B31BE7EC05B1244904A0DA144A1F2F7EAB4BDE34E7A1F47D4C3E50584AA22AFB8D1CBEF"
                                "97BC783377E3249354961FB51CF990BB7A9AC09234BB4C6981FF0FBD6042B2938BE71182DECED149F7"
                                "48F8D90C082E178DD95FA1F4FEF6D4C8CE8C050C08279B240414BB55DD622B4944A22333D7DDA0AFCE"
                                "B3DBB2DAC43D0CD26657A5613E93C9F1D041DBC867300F0B5249BAB3A629AAD99100790AC12FC6AE09"
                                "B8A01B9162E22F1E208F6D93B4BCBCECD728686C9CD43B7D004CC6540E917309B1C45FD02616A75F54"
                                "CBD8044B83C2806C68F0197C6BACAE72A8390C3CC9706943A2C9E7D5940A7039FE315D5600E67125B0"
                                "8F27A851CB24B22C9CE25C14F9F3795E7CBA518DC8756DE501980E86FCA024E5FAF04B24BC36F5339B"
                                "9D61F7E92BEA90ADFA6042169D611121F84BEF7E173E79B21A03124A321507676A500EC0BFCB1CD2B2"
                                "A08E63CF99283AC1AD07226CD406531A45454FB4B92B9FC7FA1B4A2BA3EC0B9757330FA42F3BBFF91E"
                                "4EEF33B8C3F10670BAA26701B6629F6402F13082014906092A864886F70D010701A082013A04820136"
                                "308201323082012E060B2A864886F70D010C0A0102A081F73081F4305F06092A864886F70D01050D30"
                                "52303106092A864886F70D01050C30240410F91D209284686D0515E3F9604CC5C72B02020800300C06"
                                "082A864886F70D02090500301D060960864801650304012A041088947581FDCE152D6A89157403D7B1"
                                "9404819009BFF0842C15ABE2A5142D56C66B21491A384932C5A512FB1E4EC03CF2B9C47EF06DC3E9DB"
                                "4A20F548FDA217146CC713FC9CAD482E2AC5BDC8AFD5E81AFAF4D437857554222E2A347A9682CEE3F4"
                                "B56D1EFC88B885B13FF694C073B758F837AD946B9D67F929031DAE1C94E36C24B431B2ED3CBAFDA8E4"
                                "223BEEE56D2097D0A10B52FDF9217B9EE9F8F38ED3066E6CBC3125302306092A864886F70D01091531"
                                "160414F34CB6C5D2A309187D36DFCC0B7D5A19EC81357D";

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();
    uint8_t *pwdData = nullptr;
    uint32_t pwdDataLen = 0;
    if (ConvertHex(buf, len, &pwdData, &pwdDataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    
    uint8_t *salt = (uint8_t *)BSL_SAL_Calloc(1, sizeof(g_salt));
    if (salt == nullptr) {
        BSL_SAL_Free(pwdData);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    memcpy_s(salt, sizeof(g_salt), g_salt, sizeof(g_salt));
    HITLS_PKCS12_MacData *macData = HITLS_PKCS12_MacDataNew();
    if (macData == nullptr) {
        BSL_SAL_Free(pwdData);
        BSL_SAL_Free(salt);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    macData->alg = BSL_CID_SHA256;
    macData->macSalt->data = salt;
    macData->macSalt->dataLen = sizeof(g_salt);
    macData->iteration = 10;
    uint8_t *tmpInitData = nullptr;
    uint32_t tmpInitDataLen;
    if (ConvertHex((uint8_t *)g_initData, (uint32_t)strlen(g_initData), &tmpInitData, &tmpInitDataLen) != 0) {
        BSL_SAL_Free(pwdData);
        BSL_SAL_Free(salt);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_Buffer initData = {tmpInitData, tmpInitDataLen};
    BSL_Buffer output = {0};
    BSL_Buffer pwd = {pwdData, pwdDataLen};
    HITLS_PKCS12_CalMac(&output, &pwd, &initData, macData);
    HITLS_PKCS12_MacDataFree(macData);
    BSL_SAL_Free(tmpInitData);
    BSL_SAL_Free(pwdData);
    BSL_SAL_Free(output.data);
    BSL_GLOBAL_DeInit();
    return 0;
}