/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_pkcs12_local.h"
#include "bsl_sal.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    BSL_GLOBAL_Init();
    const char *pwd = "123456";
    BSL_Buffer encPwd;
    encPwd.data = (uint8_t *)pwd;
    encPwd.dataLen = strlen(pwd);

    HITLS_PKCS12 *p12 = nullptr;

    HITLS_PKCS12_PwdParam param = {
        .macPwd = &encPwd,
        .encPwd = &encPwd,
    };
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *data1 = nullptr;
    uint32_t dataLen1 = 0;
    if (ConvertHex(buf, len, &data1, &dataLen1) != 0) {
        BSL_SAL_Free(data);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    BSL_Buffer encode = {data, dataLen};
    BSL_Buffer encode1 = {data1, dataLen1};
    HITLS_PKCS12_ParseBuff(BSL_FORMAT_ASN1, &encode, &param, &p12, true);
    HITLS_PKCS12_Free(p12);
    p12 = nullptr;
    HITLS_PKCS12_ParseBuff(BSL_FORMAT_ASN1, &encode1, &param, &p12, false);
    HITLS_PKCS12_Free(p12);
    BSL_SAL_Free(data);
    BSL_SAL_Free(data1);
    BSL_GLOBAL_DeInit();
    return 0;
}