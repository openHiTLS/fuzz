/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "bsl_errno.h"
#include "bsl_base64.h"
#include "common.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    const uint8_t *srcBuf = buf;
    const uint32_t srcLen = len;

    uint32_t encodeBufLen = HITLS_BASE64_ENCODE_LENGTH(srcLen);
    char encodeBuf[encodeBufLen];

    BSL_BASE64_Encode(srcBuf, srcLen, encodeBuf, &encodeBufLen);
    
    return 0;
}