/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "bsl_errno.h"
#include "bsl_base64.h"
#include "bsl_base64_internal.h"
#include "bsl_sal.h"
#include "common.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    const uint8_t *srcBuf = buf;
    const uint32_t srcLen = len;
    
    uint32_t encodeBufLen = HITLS_BASE64_ENCODE_LENGTH(srcLen);
    char *encodeBuf = (char *)BSL_SAL_Malloc(encodeBufLen);
    if (encodeBuf == nullptr) {
        return -1;
    }

    uint32_t tmpLen = encodeBufLen;

    BSL_Base64Ctx *ctx = BSL_BASE64_CtxNew();
    if (ctx == nullptr) {
        BSL_SAL_FREE(encodeBuf);
        return -1;
    }
    uint32_t ret = BSL_BASE64_EncodeInit(ctx);
    if (ret != BSL_SUCCESS) {
        BSL_BASE64_CtxFree(ctx);
        BSL_SAL_FREE(encodeBuf);
        return -1;
    }
    ret = BSL_BASE64_SetFlags(ctx, encodeBuf[0]);
    if (ret != BSL_SUCCESS) {
        BSL_BASE64_CtxFree(ctx);
        BSL_SAL_FREE(encodeBuf);
        return -1;
    }
    BSL_BASE64_EncodeUpdate(ctx, srcBuf, srcLen, encodeBuf, &tmpLen);
    
    BSL_BASE64_CtxFree(ctx);
    BSL_SAL_FREE(encodeBuf);
    
    return 0;
}