/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "bsl_sal.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    BSL_GLOBAL_Init();
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }

    uint8_t tag = data[0];
    BSL_ASN1_Buffer asn = {tag, dataLen - 1, data + 1};
    switch (tag) {
        case BSL_ASN1_TAG_BOOLEAN:
            {
                bool res;
                BSL_ASN1_DecodePrimitiveItem(&asn, &res);
                break;
            }
        case BSL_ASN1_TAG_INTEGER:
        case BSL_ASN1_TAG_ENUMERATED:
            {
                int32_t res;
                BSL_ASN1_DecodePrimitiveItem(&asn, &res);
                break;
            }
        case BSL_ASN1_TAG_BITSTRING:
            {
                BSL_ASN1_BitString res;
                BSL_ASN1_DecodePrimitiveItem(&asn, &res);
                break;
            }
        case BSL_ASN1_TAG_UTCTIME:
        case BSL_ASN1_TAG_GENERALIZEDTIME:
            {
                BSL_TIME res = {0};
                BSL_ASN1_DecodePrimitiveItem(&asn, &res);
                break;
            }
        case BSL_ASN1_TAG_BMPSTRING:
            {
                BSL_ASN1_Buffer decode = {tag, 0, nullptr};
                BSL_ASN1_DecodePrimitiveItem(&asn, &decode);
                BSL_SAL_FREE(decode.buff);
                break;
            }
        default:
            break;
    }
    BSL_SAL_FREE(data);
    BSL_GLOBAL_DeInit();
    return 0;
}