/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_x509_errno.h"
#include "hitls_cert_local.h"
#include "bsl_sal.h"
#include "common.h"

#define SKIP_TEST(len) \
do {                   \
    if ((len) == 0) {  \
        return 0;      \
    }                  \
} while (0)

HITLS_X509_Cmd g_format[] = {
    HITLS_X509_SET_PUBKEY,
    HITLS_X509_SET_PRIVKEY
};

static int getCsrFormat(void)
{
    int num = rand()%2;
    
    return num;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    SKIP_TEST(len);
    
    uint8_t *tmp = (uint8_t *)BSL_SAL_Malloc(len+1);
    
    (void)memcpy_s(tmp, len, buf, len);
    tmp[len] = '\0';

    HITLS_X509_Csr *csr = HITLS_X509_CsrNew();
    if (csr == nullptr) {
        BSL_SAL_FREE(tmp);
        return -1;
    }
    int format = getCsrFormat();
    HITLS_X509_CsrCtrl(csr, g_format[format], (void *)tmp, len);
    
    HITLS_X509_CsrFree(csr);
    BSL_SAL_FREE(tmp);
    
    return 0;
}