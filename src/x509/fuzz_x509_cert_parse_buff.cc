/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_x509_errno.h"
#include "hitls_cert_local.h"
#include "common.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    HITLS_X509_Cert *cert = nullptr;
    BSL_Buffer encodeRaw = {0};
    
    encodeRaw.data = (uint8_t *)buf;
    encodeRaw.dataLen = len;
    
    HITLS_X509_CertParseBuff(BSL_FORMAT_ASN1, &encodeRaw, &cert);
    
    
    HITLS_X509_CertFree(cert);
    
    return 0;
}