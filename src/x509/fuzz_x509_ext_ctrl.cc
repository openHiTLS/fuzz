/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_x509_errno.h"
#include "hitls_cert_local.h"
#include "common.h"

#define SKIP_TEST(len) \
do {                   \
    if ((len) == 0) {  \
        return 0;      \
    }                  \
} while (0)

HITLS_X509_Cmd g_format[] = {
    HITLS_X509_EXT_SET_SKI,
    HITLS_X509_EXT_SET_AKI,
    HITLS_X509_EXT_SET_KUSAGE,
    HITLS_X509_EXT_SET_SAN,
    HITLS_X509_EXT_SET_BCONS,
    HITLS_X509_EXT_SET_EXKUSAGE
};

static int getExtFormat(void)
{
    int num = rand()%2;
    
    return num;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    SKIP_TEST(len);
    HITLS_X509_Cert *cert = HITLS_X509_CertNew();
    if (cert == nullptr) {
        return -1;
    }
    
    uint8_t *pwdData = nullptr;
    uint32_t pwdDataLen = 0;
    if (ConvertHex(buf, len, &pwdData, &pwdDataLen) != 0) {
        HITLS_X509_CertFree(cert);
        BSL_SAL_FREE(pwdData);
        return 0;
    }
    
    HITLS_X509_ExtSki extSki = {0, {pwdData, pwdDataLen}};
    
    int critical = getExtFormat();
    extSki.critical = critical;
    
    HITLS_X509_Ext *ext = &cert->tbs.ext;
    
    HITLS_X509_ExtCtrl(ext, BSL_CID_CE_SUBJECTKEYID, &extSki, sizeof(HITLS_X509_ExtSki));
    
    HITLS_X509_CertFree(cert);
    BSL_SAL_FREE(pwdData);
    
    return 0;
}