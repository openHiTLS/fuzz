/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_x509_errno.h"
#include "hitls_cert_local.h"
#include "common.h"

#define SKIP_TEST(len) \
do {                   \
    if ((len) == 0) {  \
        return 0;      \
    }                  \
} while (0)

BSL_ParseFormat g_csr_format[] = {
    BSL_FORMAT_UNKNOWN,
    BSL_FORMAT_PEM,
    BSL_FORMAT_ASN1
};

static int getCsrFormat(void)
{
    int num = rand()%3;
    
    return num;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    SKIP_TEST(len);
    HITLS_X509_Csr *csr = nullptr;
    BSL_Buffer encode = {nullptr, 0};
    
    uint8_t *tmp = (uint8_t *)malloc(len);
    
    (void)memcpy_s(tmp, len, buf, len);
    tmp[len-1] = '\0';

    BSL_Buffer ori = {tmp, len};
    int idIn = getCsrFormat();
    int idOut = getCsrFormat();
    int32_t ret = HITLS_X509_CsrParseBuff(BSL_FORMAT_ASN1, &ori, &csr);
    if (ret == HITLS_X509_SUCCESS) {
        HITLS_X509_CsrGenBuff(csr, g_csr_format[idOut], &encode);
    }
    
    if(encode.data != nullptr) {
        BSL_SAL_FREE(encode.data);
    }
    
    BSL_SAL_FREE(tmp);
    HITLS_X509_CsrFree(csr);
    
    return 0;
}