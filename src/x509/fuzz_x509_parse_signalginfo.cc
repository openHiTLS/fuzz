/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "hitls_x509_local.h"
#include "bsl_sal.h"


extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    BSL_GLOBAL_Init();
    uint8_t *data = nullptr;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }

    uint8_t algBuff[9] = {0x2a, 0x86, 0x48, 0x86, 0xf7, 0x0d, 0x01, 0x01, 0x0a};
    BSL_ASN1_Buffer algId = {0, sizeof(algBuff), algBuff};
    BSL_ASN1_Buffer param = {48, dataLen, data};
    HITLS_X509_Asn1AlgId x509Alg;
    HITLS_X509_ParseSignAlgInfo(&algId, &param, &x509Alg);
    BSL_SAL_Free(data);
    BSL_GLOBAL_DeInit();
    return 0;
}