/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "bsl_obj.h"

BslCid g_arrayCid[] = {
    BSL_CID_COMMONNAME,
    BSL_CID_SURNAME,
    BSL_CID_SERIALNUMBER,
    BSL_CID_COUNTRYNAME,
    BSL_CID_LOCALITYNAME,
    BSL_CID_STATEORPROVINCENAME,
    BSL_CID_STREETADDRESS,
    BSL_CID_ORGANIZATIONNAME,
    BSL_CID_ORGANIZATIONUNITNAME,
    BSL_CID_TITLE,
    BSL_CID_GIVENNAME,
    BSL_CID_INITIALS,
    BSL_CID_GENERATIONQUALIFIER,
    BSL_CID_DNQUALIFIER,
    BSL_CID_PSEUDONYM,
    BSL_CID_DOMAINCOMPONENT,
    BSL_CID_USERID,
};

static int getCid(void)
{
    int num = rand()%17;
    
    return num;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    uint8_t *bufTmp = (uint8_t *)buf;
    
    if (len == 0) {
        return 0;
    }
    BSL_GLOBAL_Init();

    HITLS_X509_Cert *cert = HITLS_X509_CertNew();
    if (cert == nullptr || cert->tbs.issuerName == nullptr || cert->tbs.subjectName == nullptr) {
        BSL_GLOBAL_DeInit();
        return -1;
    }

    BslList *list = nullptr;
    int32_t ret = HITLS_X509_CertCtrl(cert, HITLS_X509_GET_ISSUER_DNNAME, &list, sizeof(BslList **));
    if (ret != 0 || list == nullptr || list != cert->tbs.issuerName) {
        BSL_GLOBAL_DeInit();
        HITLS_X509_CertFree(cert);
        return -1;
    }
    
    HITLS_X509_DN *dnName = (HITLS_X509_DN *)BSL_SAL_Calloc(len, sizeof(HITLS_X509_DN));
    if (dnName == nullptr) {
        BSL_GLOBAL_DeInit();
        HITLS_X509_CertFree(cert);
        return -1;
    }
    for (int i = 0; i < len; i++) {
        int id = getCid();
        dnName[i].cid = g_arrayCid[id];
        dnName[i].data = bufTmp;
        dnName[i].dataLen = len;
    }
    HITLS_X509_AddDnName(list, dnName, len);
    BSL_SAL_Free(dnName);
    HITLS_X509_CertFree(cert);
    BSL_GLOBAL_DeInit();
    
    return 0;
}