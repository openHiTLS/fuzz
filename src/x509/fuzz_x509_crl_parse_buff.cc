/*
 * This file is part of the openHiTLS/fuzz project.
 *
 * openHiTLS is licensed under the Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 *
 *     http://license.coscl.org.cn/MulanPSL2
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 */
#include <stdlib.h>
#include <stdio.h>

#include "securec.h"
#include "hitls_x509_errno.h"
#include "hitls_cert_local.h"
#include "bsl_sal.h"
#include "common.h"

BSL_ParseFormat g_crl_format[] = {
    BSL_FORMAT_UNKNOWN,
    BSL_FORMAT_PEM,
    BSL_FORMAT_ASN1
};

static int getCrlFormat(void)
{
    int num = rand()%3;
    
    return num;
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{
    HITLS_X509_Crl *crl = nullptr;
    if (len == 0) {
        return 0;
    }
    uint8_t *tmp = (uint8_t *)BSL_SAL_Malloc(len+1);
    
    (void)memcpy_s(tmp, len, buf, len);
    tmp[len] = '\0';
    
    BSL_Buffer ori = {tmp, len};

    int format = getCrlFormat();
    
    HITLS_X509_CrlParseBuff(BSL_FORMAT_PEM, &ori, &crl);
    
    HITLS_X509_CrlFree(crl);
    BSL_SAL_FREE(tmp);
    
    return 0;
}