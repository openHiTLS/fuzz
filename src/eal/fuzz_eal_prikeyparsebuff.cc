#include <stdlib.h>
#include <stdio.h>
#include "securec.h"
#include "hitls_cert_local.h"
#include "hitls_x509.h"
#include "common.h"
#include "bsl_init.h"
#include "hitls_x509_errno.h"
#include "bsl_obj.h"
#include "bsl_list.h"

extern "C"  int32_t CRYPT_EAL_PriKeyParseBuff(BSL_ParseFormat format, int32_t type, BSL_Buffer *encode,
    const uint8_t *pwd, uint32_t pwdlen, CRYPT_EAL_PkeyCtx **ealPriKey);

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *buf, uint32_t len)
{  
    BSL_GLOBAL_Init();
    uint8_t *data = NULL;
    uint32_t dataLen = 0;
    if (ConvertHex(buf, len, &data, &dataLen) != 0) {
        BSL_GLOBAL_DeInit();
        return 0;
    }
    uint8_t *encodeData = (uint8_t *)BSL_SAL_Malloc(dataLen + 1);
    if (encodeData == NULL) {
        BSL_SAL_Free(data);
        BSL_GLOBAL_DeInit();
        return 0;
    }
    (void)memcpy_s(encodeData, dataLen + 1, data, dataLen);
    encodeData[dataLen] = '\0';
    CRYPT_EAL_PkeyCtx *pkeyCtx = NULL;
    BSL_Buffer encode = {encodeData, dataLen + 1};
    const uint8_t pwd[4] = {0x31, 0x32, 0x33, 0x34};
    uint32_t pwdlen = sizeof(pwd);

    CRYPT_EAL_PriKeyParseBuff(BSL_FORMAT_UNKNOWN, CRYPT_PRIKEY_PKCS8_UNENCRYPT, &encode, NULL, 0, &pkeyCtx);
    CRYPT_EAL_PkeyFreeCtx(pkeyCtx);
    pkeyCtx = NULL;
    CRYPT_EAL_PriKeyParseBuff(BSL_FORMAT_UNKNOWN, CRYPT_PRIKEY_RSA, &encode, NULL, 0, &pkeyCtx);
    CRYPT_EAL_PkeyFreeCtx(pkeyCtx);
    pkeyCtx = NULL;
    CRYPT_EAL_PriKeyParseBuff(BSL_FORMAT_UNKNOWN, CRYPT_PRIKEY_ECC, &encode, NULL, 0, &pkeyCtx);
    CRYPT_EAL_PkeyFreeCtx(pkeyCtx);
    pkeyCtx = NULL;
    CRYPT_EAL_PriKeyParseBuff(BSL_FORMAT_UNKNOWN, CRYPT_PRIKEY_PKCS8_ENCRYPT, &encode, pwd, pwdlen, &pkeyCtx);
    CRYPT_EAL_PkeyFreeCtx(pkeyCtx);
    BSL_SAL_Free(data);
    BSL_SAL_Free(encodeData);
    BSL_GLOBAL_DeInit();
    return 0;
}