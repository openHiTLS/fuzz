#!/bin/bash

# This file is part of the openHiTLS/fuzz project.
#
# openHiTLS is licensed under the Mulan PSL v2.
# You can use this software according to the terms and conditions of the Mulan PSL v2.
# You may obtain a copy of Mulan PSL v2 at:
#
#     http://license.coscl.org.cn/MulanPSL2
#
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
# Build different miniaturized targets and perform basic functional testing.

set -eu

ROOT_PATH=$(cd $(dirname ${BASH_SOURCE[0]});pwd)
BEARFLAGS=$(which bear 2>/dev/null) || !
RUN_ARGS=''
GITCODE_LIB_URL='https://gitcode.com/openHiTLS/openhitls.git'
LIB_BRANCH='main'
SKIP_DOWNLOAD=True
SKIP_BUILD_FUZZ=False
SKIP_BUILD_LIB=False
CUSTOM_CFLAGS=''
DEFINE_CASES=''

usage()
{
    if [ -n "$0" ]; then
        printf "%-50s\n" "Error $0 Option not support."
    fi
    printf "\n"
    printf "%-50s\n" "* Script :${BASH_SOURCE[0]}"
    printf "%-50s\n" "* Usage Option : "
    printf "%-50s\n" "   -help : Help information."
    printf "%-50s\n" "   -max_len : Maximum length of a test input."
    printf "%-50s\n" "   -max_total_time : The maximum total time in seconds to run the fuzzer."
    printf "%-50s\n" "   -malloc_limit_mb : The fuzzer will exit if the target tries to allocate this number of Mb with one malloc call."
    printf "%-50s\n" "   -runs : Number of individual test runs."
    printf "%-50s\n" "   -timeout : Timeout in seconds, default 1200. If an input takes longer than this timeout, the process is treated as a failure case."
    printf "%-50s\n" "   -gitcode_url : OpenHiTLS git url."
    printf "%-50s\n" "   -lib_branch : OpenHiTLS branch."
    printf "%-50s\n" "   -no_skip_download : Not skip download."
    printf "%-50s\n" "   -skip_build_lib : Skip build lib."
    printf "%-50s\n" "   -skip_build_fuzz : Skip build fuzz."
    printf "%-50s\n" "   -add_macro : Add macro."
    printf "%-50s\n" "   -run_tests : Run test case [-run_tests=xxx|xxx]."
    printf "\n"
}

download()
{
    [[ -e "${ROOT_PATH}/openhitls" ]] && [[ ${SKIP_DOWNLOAD} == "True" ]] && return 0 || !
    [[ -e "${ROOT_PATH}/openhitls" ]] && rm -rf ${ROOT_PATH}/openhitls || !
    git clone --recurse-submodules -b ${LIB_BRANCH} ${GITCODE_LIB_URL} openhitls
    [[ "$?" != 0 ]] && exit 1 || !
}

build_lib()
{
    [[ -e "${ROOT_PATH}/openhitls/testcode/script" ]] && pushd ${ROOT_PATH}/openhitls/testcode/script || exit 1
    sed -i "/-Wformat=2/s#,##" ${ROOT_PATH}/openhitls/config/json/compile.json
    sed -i "/Wno-stringop-overread/s#\"-Wno-stringop-overread\"##g" ${ROOT_PATH}/openhitls/config/json/compile.json
    bash build_hitls.sh libfuzzer pure_c debug shared
    
    # m.daocloud.io/gcr.io/oss-fuzz-base/base-builder
    cp /usr/local/lib/clang/18/lib/x86_64-unknown-linux-gnu/libclang_rt.fuzzer.a ${ROOT_PATH}/openhitls/build/libFuzzingEngine.a
    popd
}

build_fuzz()
{
    [[ -e "${ROOT_PATH}/build" ]] && rm -rf ${ROOT_PATH}/build && mkdir ${ROOT_PATH}/build || mkdir ${ROOT_PATH}/build
    pushd ${ROOT_PATH}/build
    {
        if [ -e "${ROOT_PATH}/openhitls/build/macro.txt" ]; then
            set -- $(cat ${ROOT_PATH}/openhitls/build/macro.txt)
        fi
        eval cmake -DCUSTOM_CFLAGS=\"${CUSTOM_CFLAGS} $@\" -DCMAKE_BUILD_TYPE=Debug ..
        [[ $? == 0 ]] && make
    }
    popd
}

copy_corpus()
{
    pushd ${ROOT_PATH}/
    local testcases=($(ls -al output | awk -F/ '{print $1}' | grep -v "\." | grep -v "total " | awk '{print $NF}' | sed -e "s/^fuzz_//" | tr -s "\n" " "))
    [[ -e "${ROOT_PATH}/analysis" ]] && rm -rf ${ROOT_PATH}/analysis && mkdir ${ROOT_PATH}/analysis || mkdir ${ROOT_PATH}/analysis
    
    for i in ${!testcases[@]}
    do
        [[ -e "output/fuzz_${testcases[i]}/corpus" ]] && rm -rf "output/fuzz_${testcases[i]}/corpus" || !
        mkdir -p "output/fuzz_${testcases[i]}/corpus"
        find corpus -name "input_${testcases[i]}*" | xargs -n 1 -I NAME bash -c "cp NAME output/fuzz_${testcases[i]}/corpus"
    done
    popd
}

run()
{
    export LD_PRELOAD=/usr/lib/gcc/x86_64-linux-gnu/9/libasan.so
    
    local exes="$(find ./output/ -type d | grep -v 'corpus' | grep -v 'analysis' | awk -F/ '{print $NF}' | tr -s '\n' ' ')"
    rm -rf ./output/run.log
    for exe in ${exes[@]}
    do
        {
            echo "./output/${exe}/${exe} ./output/${exe}/corpus ${RUN_ARGS} -exact_artifact_path=./analysis/${exe}.log" >> ./output/run.log
            "./output/${exe}/${exe} ./output/${exe}/corpus ${RUN_ARGS} -exact_artifact_path=./analysis/${exe}.log 2>&1 | tee ./output/${exe}/${exe}.log"
        }&
        
        sleep 3
    done
    
    wait
    
    unset LD_PRELOAD
}

run_define_case()
{
    pushd ${ROOT_PATH}/
    if [[ -n "${DEFINE_CASES}" ]]; then
        local tmp=($(echo "${DEFINE_CASES}" | sed -e "s/|/ /g"))
        for i in ${!tmp[@]}
        do
            local case=$(find ./output/ -type f -name ${tmp[i]})
            if [[ ! -e "${case}" ]]; then
                echo "The ${case} not exist."
                exit 1
            fi
            local corpus="${case%/*}/corpus"
            eval "${case} ${corpus} ${RUN_ARGS}"
        done
    fi
    popd
}

options()
{
    local param=${1:-"-"}
    while [[ -n ${param} && ${param} != "-" ]]
    do
        local key=${param%%=*}
        local value=${param#*=}
        case "${key}" in
            -max_len)
                RUN_ARGS="${RUN_ARGS} ${key}=${value}"
                ;;
            -max_total_time)
                RUN_ARGS="${RUN_ARGS} ${key}=${value}"
                ;;
            -malloc_limit_mb)
                RUN_ARGS="${RUN_ARGS} ${key}=${value}"
                ;;
            -runs)
                RUN_ARGS="${RUN_ARGS} ${key}=${value}"
                ;;
            -timeout)
                RUN_ARGS="${RUN_ARGS} ${key}=${value}"
                ;;
            -gitcode_url)
                GITCODE_LIB_URL=${value}
                ;;
            -lib_branch)
                LIB_BRANCH=${value}
                ;;
            -no_skip_download)
                SKIP_DOWNLOAD=False
                ;;
            -skip_build_lib)
                SKIP_BUILD_LIB=True
                ;;
            -skip_build_fuzz)
                SKIP_BUILD_FUZZ=True
                ;;
            -add_macro)
                CUSTOM_CFLAGS=${value}
                ;;
            -run_tests)
                DEFINE_CASES=${value}
                ;;
            -help)
                usage
                exit 0
                ;;
            *)
                usage ${key}
                exit 1
                ;;
        esac
        shift
        param=${1:-"-"}
    done
}

options "$@"
download
[[ ${SKIP_BUILD_LIB} == "False" ]] && build_lib || !
[[ ${SKIP_BUILD_FUZZ} == "False" ]] && build_fuzz || !
copy_corpus

if [[ -z "${DEFINE_CASES}" ]]; then
    run
else
    run_define_case
fi
